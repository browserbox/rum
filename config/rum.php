<?php

return array(

    /*
     |--------------------------------------------------------------------------
     | Debugbar Settings
     |--------------------------------------------------------------------------
     |
     | Debugbar is enabled by default, when debug is set to true in app.php.
     | You can override the value by setting enable to true or false instead of null.
     |
     */
	'model'=>'User',
    'enabled' => null,
	'roles'=>array(
		'super'=>'superuser',
		'admin'=>'admin',
	),
	'view.extends'=>'app',
	
 
);
