@extends('app')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-search"></i> {{ trans('rum::users.search_title') }}</div>
		<div class="panel-body">
			{!! Form::open(array('route' => array('users.search') ,'class'=>'form-inline', 'method'=>'POST' )) !!}
				<input type="text" class="form-control" name="q" value="{{ isset($filters->text)?$filters->text:''}}" />
				<select name="state" class="form-control" >
					<option value=""  >{{ trans('rum::users.filter_by_state') }}</option>
					<option value="1" {{ isset($filters->state) && $filters->state==1?'selected':'' }} >{{ trans('rum::users.active') }}</option>
					<option value="0" {{ isset($filters->state) && $filters->state==0?'selected':'' }} >{{ trans('rum::users.inactive') }}</option>
					<option value="-1" {{ isset($filters->state) && $filters->state==-1?'selected':'' }} >{{ trans('rum::users.banned') }}</option>
				</select>
				<button type="submit" class="btn btn-primary">{{ trans('rum::users.search') }}</button>
			{!! Form::close() !!}
	</div>
</div>
<div class="table-responsive">
<table class="table table-condensed table-striped">
	<thead>
		<tr>
			<th>{{ trans('rum::users.name') }}</th>
			<th>{{ trans('rum::users.email') }}</th>
			<th>{{ trans('rum::users.state') }}</th>
			<th>{{ trans('rum::users.roles') }}</th>
			<th>{{ trans('rum::users.created_at') }}</th>
			<th>{{ trans('rum::users.last_login_at') }}</th>
			<th>{{ trans('rum::users.actions') }}</th>
		</tr>
	</thead>
	<tbody >
		@foreach($users as $user) 
		<tr>
			<td><a href="{{ route('users.show',$user->id) }}">{{ $user->name }}</a></td>
			<td>{{ $user->email }}</td>
			<td>{!! Html::userStateIcon($user->state) !!}</td>
			<td>@foreach( $user->roles as $role) 
				{{ $role->display_name }} <br />
				@endforeach
			</td>
			<td>{{ $user->created_at }}</td>
			<td>{{ $user->last_login_at }}</td>
			<td>
				<a href="{{ route('users.show',$user->id) }}" class="btn btn-info btn-xs"><i class="fa fa-search"></i> {{ trans('rum::users.view') }}</a>
				<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i> {{ trans('rum::users.block') }}</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>
@stop