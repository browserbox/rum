@extends('app')

@section('content')
	{!! Form::model($user,array('route' => array('users.update',$user->id) ,'class'=>'', 'method'=>'PUT' )) !!}
	@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif 
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-user"></i> {{ trans('rum::users.user_data') }}</div>
				<div class="panel-body">
					
						<div class="form-group">
							<label class="  control-label">{{ trans('rum::users.name') }}</label>
							<div class="controls">
								 {!! Form::text('name', old('name'), array('class'=>'form-control') ) !!} 
							</div>
						</div>

						<div class="form-group">
							<label class="control-label">{{ trans('rum::users.email') }}</label>
							<div class="controls">
								{!! Form::email('email', old('email'), array('class'=>'form-control') ) !!} 
							</div>
						</div> 
				</div> 
			</div>
			
			<div class="panel panel-default">
					<div class="panel-heading"><i class="fa fa-key"></i>  {{ trans('rum::users.avatar') }}</div>
					<div class="panel-body">  
						  <img src="http://placehold.it/300x300" alt="" class="  img-responsive" />
					</div>
				</div>
				
				
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading"><i class="fa fa-gear"></i>  {{ trans('rum::users.admin_area') }}</div>
					<div class="panel-body"> 
						 
						<h4>{{ trans('rum::users.roles') }}</h4>
						<div class="checkbox">
							<input type="hidden" name="roles" value="" />
							@foreach($roles as $role)
							<label>{!! Form::checkbox('roles['.$role->id.']',$role->id, in_array($role->id, $current_roles) ) !!} {{$role->display_name}}</label>
							@endforeach
						</div>
					</div>
				</div>
			 
				<div class="panel panel-default">
					<div class="panel-heading"><i class="fa fa-key"></i>  {{ trans('rum::users.login_area') }}</div>
					<div class="panel-body"> 
						 
						<h4>{{ trans('rum::users.passwords') }}</h4>
						 <div class="form-group">
							<label class="control-label">{{ trans('rum::users.current_password') }}</label>
							<div class="controls">
								{!! Form::password('current_password',  array('class'=>'form-control') ) !!} 
							</div>
						</div> 
						 <div class="form-group">
							<label class="control-label">{{ trans('rum::users.new_password') }}</label>
							<div class="controls">
								{!! Form::password('new_password',   array('class'=>'form-control') ) !!} 
							</div>
						</div> 
						 <div class="form-group">
							<label class="control-label">{{ trans('rum::users.new_password_confirmation') }}</label>
							<div class="controls">
								{!! Form::password('new_password_confirmation',   array('class'=>'form-control') ) !!} 
							</div>
						</div> 
					</div>
				</div>
			</div>
			 
			
			
	</div>
	<div class="form-group">
		 
			<button type="submit" class="btn btn-success">
			<i class="fa fa-save"></i> {{ trans('rum::users.update') }}
			</button>
		 
	</div>
{!! Form::close() !!}
	

@endsection
