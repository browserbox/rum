@extends('app')

@section('content') 
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/300x300" alt="" class="img-circle img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>{{ $user->name }}</h4>
                         
                        <p>
							<i class="fa fa-envelope"></i> {{ $user->email }} 
						</p>
                        <p>
							{{ trans('rum::users.created_at') }} {{ $user->created_at }}<br />
							{{ trans('rum::users.last_login_at') }} {{ $user->last_login_at }}<br />
						</p>
                        <a href="{{ route('users.edit',$user->id) }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> {{ trans('rum::users.edit') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
 
@stop