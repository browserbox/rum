<?

Html::macro('userStateIcon', function($value)
{
    switch($value) {
		case 1:
			return '<i title="'.trans('rum::users.active').'" class="fa fa-check icon-success"></i>';
			break;
		case 0:
			return '<i title="'.trans('rum::users.inactive').'"  class="fa fa-minus icon-neutral"></i>';
			break;
		case -1:
			return '<i title="'.trans('rum::users.banned').'"  class="fa fa-ban icon-danger"></i>';
	}
});