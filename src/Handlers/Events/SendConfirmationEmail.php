<?php namespace Browserbox\Rum\Handlers\Events;

use Browserbox\Rum\Events\UserRegistred;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class SendConfirmationEmail {
	
	public function __construct() {
		
	}
	
	
	public function handle(UserRegistred $event) {
		   
		 $new_user = $event->user;
		 $data = array( 'user'=>$new_user);
		\Mail::queue('rum::emails.activation', $data , function($message) use ($event)
		{
			$message->to($event->user->email,$event->user->name)->subject(trans('rum::emails.activation') );
		});
	}
}