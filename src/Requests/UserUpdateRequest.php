<?php namespace Browserbox\Rum\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest {

	//
	public function rules()
	{
		$userId = $this->route()->getParameter('users');
		return [
			'email' => 'required|unique:users,email,'.$userId.'|max:255',
			'name' => 'required',
			'password' => 'sometimes|required',
			'new_password' => 'required|confirmed', 
		];
	}
	
	public function authorize()
    {
        return true;
    }

}
