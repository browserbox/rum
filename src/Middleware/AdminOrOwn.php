<?php namespace Browserbox\Rum\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AdminOrOwn  {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$id = $request->route('users') ;
		if( ! $this->auth->user()->hasRole( config('rum.roles.super') , config('rum.roles.admin') ) && $this->auth->user()->id != $id) {
				if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect('/')->with('message','Not allowed here');
			}
		}
		\Debugbar::info($request->route('users') ); 

		return $next($request);
	}

}
