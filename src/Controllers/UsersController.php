<?php namespace Browserbox\Rum\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Browserbox\Rum\Models\User as User;
use Browserbox\Rum\Models\Role as Role;
use Browserbox\Rum\Requests\UserUpdateRequest as UserUpdateRequest;


class UsersController extends BaseController
{
	
	public function __construct() {
		$this->middleware('auth',['except' => [ 'register', 'activate','forgot']]);
		$this->middleware('auth.admin',['except' => ['edit','show','register', 'activate','forgot']]);
		$this->middleware('auth.adminOrOwn',['only' => ['edit','show']]);
		
		
	}
    public function index() {
		$user = \Auth::user();
		
		$users = User::cansee()->filtered()->get();
		$filters = \Session::get('filters',null);
		
		return \View::make('rum::users.index', compact('users','filters'));
	}
	public function search() {
		$filters = new \stdClass;
		$state = \Request::input('state',"");
		$text = \Request::input('q',"");
		if( $state !== "" && $state != null)
			$filters->state = $state;
		if( $text !== "" && $text != null)
			$filters->text = $text;
		\Session::set('filters',$filters);
		return redirect()->route('users.index');
	}
    public function show($id) {
		$user = User::findOrFail($id);
		return \View::make('rum::users.show', compact('user'));
	}
    public function edit($id) {
		$user = User::findOrFail($id);
		$roles = Role::all();
		$current_roles = $user->roles->lists('id');
		return \View::make('rum::users.edit', compact('user','roles','current_roles'));
	}
	
	public function update($id, UserUpdateRequest $request) {
		// $data = $request;
		$data = $request->all();
		\Debugbar::info($data);
		$user = User::findOrFail($id);
		
		if(  trim($data['new_password']) != "" ) {
			$data['password'] = \Hash::make($data['new_password']);
		}
		unset($data['new_password_confirmation']);
		unset($data['new_password']);
		if( isset($data['current_password']) ) {
			unset($data['current_password']);
		}
		$user->fill($data);
		$user->save();
		$user->roles()->sync($data['roles'] );
		return \Redirect::route( 'users.show',$id );
	}
}
