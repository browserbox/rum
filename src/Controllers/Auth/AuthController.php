<?php namespace Browserbox\Rum\Controllers\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Browserbox\Rum\Events\UserRegistred;
use Browserbox\Rum\Models\User as rumUser;


class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}
	
	public function getRegister()
	{
		return view('rum::users.register');
	}
	
	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}
		$user = $this->registrar->create($request->all());
		$user->state = 0;
		$user->activation_token = hash_hmac('sha256', str_random(40), config('app.key') );
		$user->save();
		// $this->auth->login();
		\Event::fire(new UserRegistred($user));
		return redirect($this->redirectPath())->withMessage( trans('rum::user.created_ok') );
	}

	
	public function postLogin(Request $request)
    {

         $this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);
		$credentials = $request->only('email', 'password');
		
		if (\Auth::attempt($credentials, $request->has('remember')))
		{
			$user = \Auth::user();
			
			if( $user->state == 0) {
				\Auth::logout();
				return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						 trans('rum::auth.not_active'),
					]);
			} else if( $user->state == -1) {
				\Auth::logout();
				return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						  trans('rum::auth.banned'),
					]);
			}
			$user->last_login_at =date("Y-m-d H:i:s");
			$user->save();
			return redirect()->intended($this->redirectPath());
		}
		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
					]);

    }
	
	public function getActivate($activation_token) {
		if( ! $activation_token )
			return redirect('auth/login')->with('message', trans('rum::auth.token_not_found') );
		// \Debugbar::info($this->registrar);
		$user = rumUser::where('activation_token',$activation_token)->first();
		if( ! $user ) {
			return redirect('auth/login')->with('message',  trans('rum::auth.token_not_found') );
		}
		if( $user->state == 0) {
			$user->state = 1;
			$user->activation_token = '';
			$user->save();
			// Session
			return redirect('auth/login')->with('message',  trans('rum::auth.activation_success') );
		}
	}
}
