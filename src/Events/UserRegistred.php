<?php namespace Browserbox\Rum\Events;

use \App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserRegistred extends Event {
	use SerializesModels;
	
	public $user;
	
	public function __construct($user) {
		$this->user = $user;
	}
}