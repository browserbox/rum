<?php namespace Browserbox\Rum\Models;


use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use  Zizaco\Entrust\Traits\EntrustUserTrait as EntrustUserTrait;

class User extends Model  implements AuthenticatableContract, CanResetPasswordContract  {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	public function scopeCanSee($query) {
		$user = \Auth::user();
		if( $user->hasRole( config('rum.roles.super') ) ) {
			
		} else if ($user->hasRole('admin') ) { 
			
			$query = $query->whereNotExists(
			function($query)  
            {
				$super = Role::where('name','=', config('rum.roles.super') )->first();
 
                $query->select(\DB::raw(1))
                      ->from('role_user')
                      ->whereRaw('role_user.user_id = users.id')->where('role_user.role_id',$super->id);
            });
			 
		} else
			$query = $query->where('id',$user->id);
		return $query;
	}
	
	public function scopeFiltered($query) {
		$filters = \Session::get('filters');
		
		if( isset($filters->state) )
			$query = $query->where('state',$filters->state);
		if( isset($filters->text) ) {
			$text = $filters->text;
			
			$query = $query->where( function($q) use ($text) {
			 
				$q->orWhere('name','like','%'.$text.'%')
					->orWhere('email','like','%'.$text.'%');
			});
		}
		return $query;
		// $query->where('
	}

}
